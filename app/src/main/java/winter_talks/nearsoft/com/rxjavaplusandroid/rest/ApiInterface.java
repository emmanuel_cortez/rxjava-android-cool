package winter_talks.nearsoft.com.rxjavaplusandroid.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import winter_talks.nearsoft.com.rxjavaplusandroid.model.Movie;
import winter_talks.nearsoft.com.rxjavaplusandroid.model.MoviesResponse;


public interface ApiInterface {
    @GET("movie/top_rated")
    Call<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<Movie> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
}
