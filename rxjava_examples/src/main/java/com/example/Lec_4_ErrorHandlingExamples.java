package com.example;


import rx.Observable;

/**
 * Created by ecortez on 2/27/17.
 */

public class Lec_4_ErrorHandlingExamples {

    public static void main(String[] args) {

        Lec_1_HelloWorld.printTitle("Error Handling example");

        Observable.just("Passing String")
                .map(s -> potentialException(s))
                .map(s -> anotherPotentialException(s))
                .subscribe(string -> System.out.println(string)
                        , throwable -> System.out.println(throwable)
                        , () -> System.out.println("Completed!!!"));
    }


    private static String potentialException(String string) {
        //int num = 12/0; //Causing error on purpose
        return string;
    }

    private static String anotherPotentialException(String string) {
        String[] stringArray = new String[]{};

        //String myString= stringArray[2]; //Causing error on purpose
        return string;
    }
}
