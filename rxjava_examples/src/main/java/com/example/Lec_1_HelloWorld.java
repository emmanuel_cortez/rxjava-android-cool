package com.example;


import rx.Observable;
import rx.Observer;
import rx.Subscriber;

public class Lec_1_HelloWorld {

    public static void main(String[] args) {

        printTitle("First RxJava example");

        Observable<String> greetingsObservable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("Hello World!!!");
                subscriber.onCompleted();
            }
        });

        Observer<String> mySubscriber = new Observer<String>() {

            @Override
            public void onNext(String value) {
                System.out.println(value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onCompleted() {
                System.out.println("EOM.");
            }
        };

        greetingsObservable.subscribe(mySubscriber);

        /*******************************************************/
        //Equivalent using lambda expression for BEAUTY
        /*******************************************************/

        printTitle("Same example...(But prettier)");

        Observable<String> shortGreetingsObservable = Observable.create(subscriber -> {
            subscriber.onNext("Hello World!!!");
            subscriber.onCompleted();
        });

        shortGreetingsObservable.subscribe(
                s -> System.out.println(s),
                throwable -> {
                },
                () -> System.out.println("EOM.")
        );
    }

    public static void printTitle(String title) {
        System.out.println();
        System.out.println();
        System.out.println("/**********************************************************/");
        System.out.println("\t" + title);
        System.out.println("/**********************************************************/");
        System.out.println();

    }
}
