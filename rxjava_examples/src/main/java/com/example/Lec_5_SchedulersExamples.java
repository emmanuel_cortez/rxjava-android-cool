package com.example;


import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by ecortez on 2/27/17.
 */

public class Lec_5_SchedulersExamples {

    public static void main(String[] args) {

        //http://reactivex.io/documentation/scheduler.html
        Lec_1_HelloWorld.printTitle("Scheduler example: Same thread");

        System.out.println("Starting on threadId: " + Thread.currentThread().getId());

        Thread.currentThread().setName("Main Thread");
        getNumbers()
                .flatMap(numbers -> {
                    System.out.println("FlatMap Thread = " + Thread.currentThread().getName());
                    return Observable.from(numbers);
                })
                .map(number -> {
                    System.out.println("Map Thread = " + Thread.currentThread().getName());
                    return number % 2;
                })
                .forEach(number -> {
                    System.out.println("ForEach Thread = " + Thread.currentThread().getName());
                    System.out.println("Is Odd " + (number == 0));
                });


        Lec_1_HelloWorld.printTitle("Scheduler example: Multi-thread 1");

        Thread.currentThread().setName("Main Thread");
        getNumbers()
                .observeOn(Schedulers.newThread())
                .flatMap(numbers -> {
                    System.out.println("FlatMap Thread = " + Thread.currentThread().getName());
                    return Observable.from(numbers);
                })
                .map(number -> {
                    System.out.println("Map Thread = " + Thread.currentThread().getName());
                    return number % 2;
                })
                .toBlocking()
                .forEach(number -> {
                    System.out.println("ForEach Thread = " + Thread.currentThread().getName());
                    System.out.println("Is Odd " + (number != 0));
                });


        Lec_1_HelloWorld.printTitle("Scheduler example: Multi-thread 2");

        Thread.currentThread().setName("Main Thread");
        getNumbers()
                .subscribeOn(Schedulers.newThread())
                .flatMap(numbers -> {
                    System.out.println("FlatMap Thread = " + Thread.currentThread().getName());
                    return Observable.from(numbers);
                })
                .map(number -> {
                    System.out.println("Map Thread = " + Thread.currentThread().getName());
                    return number % 2;
                })
                .toBlocking()
                .forEach(number -> {
                    System.out.println("ForEach Thread = " + Thread.currentThread().getName());
                    System.out.println("Is Odd " + (number != 0));
                });
    }


    private static Observable<Integer[]> getNumbers() {
        System.out.println("Function getNumbers Thread = " + Thread.currentThread().getName());
        return Observable.create(
                subscriber -> {
                    System.out.println("Observable.create Thread = " + Thread.currentThread().getName());
                    Integer[] numbers = {3, 6, 2};
                    subscriber.onNext(numbers);
                    subscriber.onCompleted();
                }
        );
    }
}
