package com.example;

import java.util.Arrays;
import java.util.List;

import rx.Observable;

/**
 * Created by ecortez on 2/23/17.
 */

public class Lec_2_CreatingObservables {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("Android", "Ubuntu", "Mac OS", "Windows");

        Lec_1_HelloWorld.printTitle("Using 'JUST'");

        Observable<List<String>> listObservable = Observable.just(list);

        listObservable.subscribe(strings -> System.out.println(strings));


        Lec_1_HelloWorld.printTitle("Using 'FROM CALLABLE'");

        Observable<String> awesomeObservable = Observable.fromCallable(() -> {
            String result = doSomeTimeTakingIoOperation();

            return result.toUpperCase();
        });

        awesomeObservable.subscribe(s -> System.out.println(s));

        Lec_1_HelloWorld.printTitle("Using 'CREATE'");

        Observable<String> otherAwesomeObservable = Observable.create(subscriber -> {
            try {
                String result = doSomeTimeTakingIoOperation();
                subscriber.onNext(result);    // Pass on the data to subscriber
                subscriber.onCompleted();     // Signal about the completion subscriber
            } catch (Exception e) {
                subscriber.onError(e);        // Signal about the error to subscriber
            }
        });


        otherAwesomeObservable.subscribe(s -> System.out.println(s));

    }

    static String doSomeTimeTakingIoOperation() {
        return "Doing something awesome...";
    }


}