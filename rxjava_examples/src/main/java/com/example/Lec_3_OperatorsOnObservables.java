package com.example;

import java.util.Arrays;
import java.util.List;

import rx.Observable;

/**
 * Created by ecortez on 2/26/17.
 */

public class Lec_3_OperatorsOnObservables {


    public static void main(String[] args) {

        List<String> words = Arrays.asList(
                "the",
                "quick",
                "brown",
                "fox",
                "jumped",
                "over",
                "the",
                "lazy",
                "dog"
        );

        //  Map
        //  http://reactivex.io/documentation/operators/map.html

        Lec_1_HelloWorld.printTitle("Map example");

        Observable.from(words)
                .map(word -> word.toUpperCase())
                .subscribe(System.out::println);


        //  FlatMap
        //  http://reactivex.io/documentation/operators/flatmap.html

        Lec_1_HelloWorld.printTitle("FlatMap example");


        Observable.from(words)
                .flatMap(word -> toUpperSplitted(word))
                .subscribe(strings -> {
                    for (String letter : strings) {
                        System.out.println(letter);
                    }
                });

        //  ZipWith:
        //  http://reactivex.io/documentation/operators/zip.html

        Lec_1_HelloWorld.printTitle("ZipWith example");

        Observable.from(words)
                .zipWith(Observable.range(1, Integer.MAX_VALUE),
                        (string, count) -> String.format("%2d. %s", count, string))
                .subscribe(System.out::println);


        //  Filter:
        //  http://reactivex.io/documentation/operators/filter.html

        Lec_1_HelloWorld.printTitle("Filter example");

        Observable.from(words)
                .filter(word -> word.length() > 3)
                .zipWith(Observable.range(1, Integer.MAX_VALUE),
                        (string, count) -> String.format("%2d. %s", count, string))
                .subscribe(System.out::println);

    }

    private static Observable<String[]> toUpperSplitted(String word) {
        return Observable.fromCallable(() -> word.toUpperCase().split(""));
    }


}
