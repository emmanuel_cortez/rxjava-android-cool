# RxJava+Android = COOL #
El propósito de este taller es dar una introducción al desarrollo de aplicaciones Android usando la librería de programación funcional-reactiva RxJava.

Los siguientes ejercicios conducirán al usuario en la modificación de un pequeño proyecto vosualizador de películas en Android a modo que pueda experimentar algunas de las venajas de implementar esta librería y este cambio de paradigma. 

## Ejercicio 1: Incluir dependencias ##

Las dependencias que se incluirán, por el momento, son las siguientes:
1. Rxjava (the basic dependenci for using RxJava)
2. RxAndroid (Which has some useful utilities for managing observables and schedulers in Adroid)
3. RxBinding (Contiene métodos cómodos para obtener Observables emitidos por los diferentes widgets de Android)

Para ello sólo tenemos que incluir las siguientes 3 líneas en el archivo app/build.gradle:


```
#!Groovy

    //RxJava
    compile "io.reactivex:rxjava:1.2.0"
    compile "io.reactivex:rxandroid:1.2.1"
    compile "com.jakewharton.rxbinding:rxbinding:0.4.0"
```

## Ejercicio 2: Crear un Subscription Manager ##

El Subscription Manager es una clase que nos permitirá administrar las subscripciones que nuestra aplicación mantenga abiertas. Al ser integrado al ciclo de vida de nuestras actividades, este ayudará a evitar fugas de memoria potenciales.

A continuación añadiremos la clase "SubscriptionManager" al paquete "application":


```
#!Java

class SubscriptionManager {
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    private boolean isActive;

    public void start() {
        isActive = true;
    }

    public void stop() {
        isActive = false;
        compositeSubscription.clear();
    }

    public boolean bind(Subscription pSubscription) {
        if (isActive) {
            compositeSubscription.add(pSubscription);
        } else {
            pSubscription.unsubscribe();
        }
        return !pSubscription.isUnsubscribed();
    }

    public void remove(Subscription pSubscription) {
        compositeSubscription.remove(pSubscription);
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean hasSubscriptions() {
        return compositeSubscription.hasSubscriptions();
    }
}
```

A continuación hemos de integrarla al ciclo de vida de nuestras actividades incluyéndola en nuestra BaseActivity:


```
#!Java

    private SubscriptionManager subscriptionManager = new SubscriptionManager();

    @Override
    protected void onResume() {
        super.onResume();
        subscriptionManager.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        subscriptionManager.stop();
    }

    protected boolean bind(Subscription pSubscription) {
        return subscriptionManager.bind(pSubscription);
    }
```

## Ejercicio 3: Crear un click observable para vistas ##

Nos situaremos en la actividad que muestra los detalles de cada película (activity/DetailsActivity.java) y extraemos la declaración del OnClickListener del botón "tubeButton" en una función que arroje un observable:


```
#!Java

    private void subscribeTubeButtonClick() {
        bind(RxView.clicks(tubeButton)
                .subscribe(aVoid -> {
                    String queryTitle = movie.getTitle().replace(" ", "+");
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/results?search_query=" + queryTitle)));
                })
        );
    }
```

La ejecución de este método deberá realizarse en el evento "onResume" de nuestra actividad:

```
#!java

    @Override
    protected void onResume() {
        super.onResume();
        subscribeTubeButtonClick();
    }
```
 de modo que la subscripción a este Observable pueda realizarse cada vez que la actividad sea accedida.

## Ejercicio 4: Crear un click observable para Adapters en un RecyclerView ##

La solución empleada para proveer de un OnClickListener a los elementos de un ReciclerView consistía en introducir un OnClikListener desde la actividad que hiciera uso del adaptador, ahora lo que haremos será ofrecer un Observer a partir del cual pueda iniciarse el flujo de procesamiento de las acciones que queramos llevar a cabo al dar click en cada elemento de la lista de películas.

Para ello, lo primero que haremos es declarar un objeto PublishSubject ([Más sobre Subjects aqui](http://reactivex.io/documentation/subject.html)) en el adaptador de nuestro RecyclerView:

```
#!java
    private PublishSubject<Movie> onClickSubject = PublishSubject.create();

```

Ahora, reemplazamos la invocación del método "holder.bind" que se encuentra en el enlace del ViewHolder con la siguiente implementación en RxJava:

```
#!java

        holder.itemView.setOnClickListener(v -> onClickSubject.onNext(movies.get(position)));
```
No nos olvidemos de crear un getter para obtener este Subject CONVIRTIENDOLO en Observable:

```
#!java

    public Observable<Movie> getItemClicks() {
        return onClickSubject.asObservable();
    }
```

**Nota:** No olvidemos eliminar los métodos que queden sin invocar; esto con la finalidad de tener un código limpio y legible.

Por último, extraeremos la declaración del RecyclerView para que sea un atributo privado de clase; al igual que su adaptador:

```
#!java

    private RecyclerView recyclerView;
    private MoviesAdapter moviesAdapter;
```

y creamos un método que realize la subscripción del Observable arrojado por los clicks realizados en los elementos de la lista de películas:

```
#!java
    private void subscribeRecyclerViewerItemClicks() {
        if (moviesAdapter != null) {
            bind(moviesAdapter.getItemClicks()
                    .subscribe(movie -> {
                        Log.e(TAG, movie.getTitle() + ", " + movie.getId());

                        Intent details = new Intent(MainActivity.this, DetailsActivity.class);
                        details.putExtra(DetailsActivity.MOVIE_ID, movie.getId());

                        startActivity(details);
                    })
            );
        }
    }
```

Ahora que el constructor de nuestro adaptador ha cambiado, es necesario modificar la acción realizada al momento de recibir la lista de películas (sin olvidar invocar nuestro método "subscriptor" justo después de la creación del adaptador:

```
#!java

        moviesAdapter = new MoviesAdapter(movies);
        recyclerView.setAdapter(moviesAdapter);

        subscribeRecyclerViewerItemClicks();
```

No olvidemos invocar dicho método "subscriptor" en el evento "onResume" de nuestra actividad (es necesario hacerlo para recuperar su funcionalidad al regresar de la pantalla de detalles):


```
#!java

    @Override
    protected void onResume() {
        super.onResume();
        subscribeRecyclerViewerItemClicks();
    }
```


## Ejercicio 5: Implementar la API Rest usando RxJava ##

Nuestra API está implementada usando Retrofit, librería que resulta muy útil debido a su versatilidad. En esta ocasión nos valdremos de esta propiedad para facilitar el consumo de servicios web usando una extensión de retrofit cuyo objetivo es adaptar las llamadas a Observables:

```
#!groovy

compile "com.squareup.retrofit2:adapter-rxjava:2.0.2"
```

A continuación, iremos a la clase /rest/ApiClient.java y añadiremos la declaración del adaptador de llamadas para el cliente Retrofit (hay que hacerlo antes de la sentencia "build()"):


```
#!java

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create()) //Linea agregada
                    .build();
```

Acto seguido, podemos indicar que nuestra Interfaz de la API de servicios web arroja Observables en lugar de Calls en cada llamada (/rest/ApiInterface.java):

```
#!java
    @GET("movie/top_rated")
    Observable<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Observable<Movie> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
```

Dirigiéndonos a la actividad principal, hemos de abstraer las acciones realizadas al recibir la lista de películas en un método privado (esto es más limpio y legible):

```
#!java
    private void fillData(MoviesResponse response) {
        List<Movie> movies = response.getResults();
        moviesAdapter = new MoviesAdapter(movies);
        recyclerView.setAdapter(moviesAdapter);

        subscribeRecyclerViewerItemClicks();
    }
```

También vamos a extraer la llamada al servicio rest que obtiene la lista en un método "subscriptor":

```
#!java
    private void subscribeGetMovieList() {
        if (moviesAdapter == null) {
            bind(getApiService().getTopRatedMovies(API_KEY)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(moviesResponse -> fillData(moviesResponse)
                            , throwable -> Log.e(TAG, throwable.toString())));
        }
    }
```
es preciso añadir este método al evento "onResume" de nuestra actividad para que pueda ser invocado al ingresar por primera vez a la aplicación:

```
#!java
    @Override
    protected void onResume() {
        super.onResume();
        subscribeGetMovieList();
        subscribeRecyclerViewerItemClicks();
    }
```


## Ejercicio 6: Realizar la implementación de la API Rest en la pantalla de Detalles de película  ##

Los pasos a seguir son similares a los del ejercicio 5, y siempre podrán analizar la implementación de estos ejercicios contenida en la rama "resuelto" de este proyecto.